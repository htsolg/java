package ua.rv.huts.oleh.phone;

public class MotorolaPhone extends Phone implements PhoneConnection, PhoneMedia{

    public MotorolaPhone(String name, String model, double storageCapacity, double ramVolume) {
        super(name, model, storageCapacity, ramVolume);
    }

    public void call() {
        System.out.println("Motorola call!!!");
    }

    @Override
    public void sendAMessage() {
        System.out.println("Motorola send message.");
    }

    @Override
    public void takePhotos() {
        System.out.println("Motorola take a photos");
    }

    @Override
    public void shootVideo() {
        System.out.println("Shooting video using Motorola");
    }
}
