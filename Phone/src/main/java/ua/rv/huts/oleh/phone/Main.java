package ua.rv.huts.oleh.phone;

public class Main {

    public static void main(String[] args) {
        MotorolaPhone motorola = new MotorolaPhone("Motorola", "G60", 256, 16);
        NokiaPhone nokia = new NokiaPhone("Nokia", "6310", 0.016, 0.008);
        SamsungPhone samsung = new SamsungPhone("Samsung", "S22", 128, 8);

        motorola.call();
        motorola.sendAMessage();
        motorola.shootVideo();
        nokia.call();
        samsung.takePhotos();

    }
}