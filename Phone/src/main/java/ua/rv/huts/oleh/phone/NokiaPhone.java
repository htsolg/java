package ua.rv.huts.oleh.phone;

public class NokiaPhone extends Phone implements PhoneConnection {
    public NokiaPhone(String name, String model, double storageCapacity, double ramVolume) {
        super(name, model, storageCapacity, ramVolume);
    }

    @Override
    public void call() {
        System.out.println("Nokia call!!!");
    }

    @Override
    public void sendAMessage() {
        System.out.println("Message from Nokia.");
    }


}
