package ua.rv.huts.oleh.phone;

public class SamsungPhone extends Phone implements PhoneConnection, PhoneMedia {
    public SamsungPhone(String name, String model, double storageCapacity, double ramVolume) {
        super(name, model, storageCapacity, ramVolume);
    }

    @Override
    public void call() {
        System.out.println("ZZZZZ.... Samsung call!!!");
    }

    @Override
    public void sendAMessage() {
        System.out.println("Samsung send message.");
    }

    @Override
    public void takePhotos() {
        System.out.println("Samsung take a photos");
    }

    @Override
    public void shootVideo() {
        System.out.println("Shooting video using Samsung");
    }
}
