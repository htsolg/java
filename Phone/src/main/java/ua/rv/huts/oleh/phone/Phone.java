package ua.rv.huts.oleh.phone;

public abstract class Phone {

    String name;
    String model;
    double storageCapacity;
    double ramVolume;


    Phone(String name){
        this.name = name;
    }

    Phone(String name, String model){
        this.name = name;
        this.model = model;
    }


    Phone(String name, String model, double storageCapacity){
        this.name = name;
        this.model = model;
        this.storageCapacity = storageCapacity;
    }

    Phone(String name, String model, double storageCapacity, double ramVolume) {
        this.name = name;
        this.model = model;
        this.storageCapacity = storageCapacity;
        this.ramVolume = ramVolume;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getStorageCapacity() {
        return storageCapacity;
    }

    public void setStorageCapacity(double storageCapacity) {
        this.storageCapacity = storageCapacity;
    }

    public double getRamVolume() {
        return ramVolume;
    }

    public void setRamVolume(double ramVolume) {
        this.ramVolume = ramVolume;
    }



}


