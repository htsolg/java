package com.company.professions;

import com.company.entities.Person;

public class Driver extends Person {

    private int drivingExperience;

    public Driver(String fullName, int age, String gender, String phoneNumber, int drivingExperience) {
        super(fullName, age, gender, phoneNumber);
        this.drivingExperience = drivingExperience;
    }

    public int getDrivingExperience() {
        return drivingExperience;
    }

    public void setDrivingExperience(int drivingExperience) {
        this.drivingExperience = drivingExperience;
    }
}
