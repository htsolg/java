package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class SportCar extends Car{

    int maxSpeed;

    public SportCar(String brand, String model, float weight, Driver driver, Engine engine, int maxSpeed) {
        super(brand, model, weight, driver, engine);
        this.maxSpeed = maxSpeed;
    }
}
