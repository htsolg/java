package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Car {
    private String brand;
    private String model;
    public float weight;

    Driver driver;
    Engine engine;

    public Car(String brand, String model, float weight, Driver driver, Engine engine) {
        this.brand = brand;
        this.model = model;
        this.weight = weight;
        this.driver = driver;
        this.engine = engine;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }



    //Методи
    public void start() {
        System.out.println("Поїхали");
    };

    public void stop() {
        System.out.println("Зупиняємося");
    };

    public void turnRight() {
        System.out.println("Поворот направо");
    };

    public void turnLeft() {
        System.out.println("Поворот наліво");
    };


    @Override
    public String toString(){
        return " Car%nBrand: %s, model: %s, weight: %.2f kg.%n"
                .formatted(brand, model, weight) +

                (" Driver%n" +
                        "Full name: %s, age: %d, gender: %s, phone number: %s").formatted(getDriver().getFullName(),
                        getDriver().getAge(), getDriver().getGender(), getDriver().getPhoneNumber()) +

                ("%n Engine%n" +
                        "power: %.2f kW, manufacturer: %s")
                        .formatted(getEngine().getPower(), getEngine().getManufacturer());
    }
}
