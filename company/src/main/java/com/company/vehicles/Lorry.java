package com.company.vehicles;

import com.company.details.Engine;
import com.company.professions.Driver;

public class Lorry extends Car{

    float carryingСapacity;

    public Lorry(String brand, String model, float weight, Driver driver, Engine engine, float carryingСapacity) {
        super(brand, model, weight, driver, engine);
        this.carryingСapacity = carryingСapacity;
    }
}
