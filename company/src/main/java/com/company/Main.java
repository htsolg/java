package com.company;

import com.company.details.Engine;
import com.company.professions.Driver;
import com.company.vehicles.Car;

public class Main {
    public static void main(String[] args) {

        Driver driver = new Driver("Van Dam", 34, "male", "888-307-665",12);
        Engine engine = new Engine(176, "Germany");
        Car car = new Car("WV", "Tiguan", 1699, driver, engine);

        System.out.println(car);
    }
}