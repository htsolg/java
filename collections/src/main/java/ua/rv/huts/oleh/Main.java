package ua.rv.huts.oleh;

import java.util.*;

public class Main {
    public static void main(String[] args) {

        String [] arrayOfStudents = {"Антонов", "Шевченко", "Бойко", "Шевченко", "Кравченко", "Савицький", "Антонов",
                "Шевченко", "Тетерів", "Мамченко"};


        List<String> groupOfStudents = new ArrayList<>();
        for(String person: arrayOfStudents){
            groupOfStudents.add(person);
        }
        System.out.println("Number of items in groupOfStudents is: " + groupOfStudents.size());


        Set<String> setOfSurname = new TreeSet<>();
        for(String person: arrayOfStudents){
            setOfSurname.add(person);
        }
        System.out.println("Number of unique surnames is: " + setOfSurname.size());


        Map<String, Integer> mapStudents = new TreeMap<>();
        for(String person: arrayOfStudents){
            mapStudents.put(person, person.length());
        }
        System.out.println(mapStudents.toString());
    }
}